<?php

use GuzzleHttp\Psr7\ServerRequest;
use JPinto\TumbleweedCache\CacheFactory;

require_once __DIR__ . '/../vendor/autoload.php';


$reqs = 500000;


$start = microtime(true);
for ($n = 0; $n <= $reqs;$n++) {
    for ($i = 0; $i <= 10; $i++) {
//        $route_regex = preg_replace('@:[^/]+@', '([^/]+)', '/api/products/:sku');
//        preg_match('@' . $route_regex . '@', '/api/products/123', $matches);
        preg_match('@/api/products/([^/]+)/very/long/path/and/some/more@', '/api/products/123/very/long/path/and/some/more', $matches);

    }
    $request = ServerRequest::fromGlobals();
}
$end = microtime(true);
echo 'preg matching: ' . number_format($reqs/($end-$start), 0) . ' req/s' . PHP_EOL;



$start = microtime(true);
for ($n = 0; $n <= $reqs;$n++) {
    for ($i = 0; $i <= 10; $i++) {
        isMatch('/api/products/123/very/long/path/and/some/more', '/api/products/:sku/very/long/path/and/some/more');
    }
    $request = ServerRequest::fromGlobals();

}
$end = microtime(true);
echo 'matching: ' . number_format($reqs/($end-$start), 0) . ' req/s' . PHP_EOL;

$cache = CacheFactory::make();
$cache->save($cache->getItem(str_replace('/', '_', '/api/products/123/very/long/path/and/some/more'))->set('/api/products/:sku'));

$start = microtime(true);
for ($n = 0; $n <= $reqs;$n++) {
    $item = $cache->getItem(str_replace('/', '_', '/api/products/123/very/long/path/and/some/more'));
    $item->isHit();
    $request = ServerRequest::fromGlobals();

}
$end = microtime(true);
echo 'from cache: ' . number_format($reqs/($end-$start), 0) . ' req/s' . PHP_EOL;

$start = microtime(true);
for ($n = 0; $n <= $reqs;$n++) {
    apc_fetch(str_replace('/', '_', '/api/products/123/very/long/path/and/some/more'));
    $request = ServerRequest::fromGlobals();
}
$end = microtime(true);
echo 'from direct cache: ' . number_format($reqs/($end-$start), 0) . ' req/s' . PHP_EOL;


function isMatch($url, $route)
{
    $filterPath = explode('/', $route);
    $urlPath = explode('/', $url);

    // short circuit its path has diff components
    if (count($filterPath) !== count($urlPath)) {
        return false;
    }

    $data['parameters'] = array();

    foreach($filterPath as $i => $key) {
        if (strpos($key, ':') === 0) {
            $data['parameters'][substr($key, 1)] = $urlPath[$i];
        }

        // this filter is irrelevent
        else if($key !== $urlPath[$i])
        {
            return false;
        }
    }

    return $data;
}
