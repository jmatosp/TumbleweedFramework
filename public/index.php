<?php
/**
 * Web App Template
 *
 * Components:
 *      Guzzle/Psr7 - Provides HTTP Request/Response with Psr7 interface
 *      JPinto/TumbleweedCache - Provides cache services with Psr6 interface
 */

use GuzzleHttp\Psr7\ServerRequest;
use JPinto\Tumbleweed\Controllers\HelloController;
use JPinto\Tumbleweed\Router\Router;

require_once __DIR__ . '/../vendor/autoload.php';

// routing
$router = new Router();
$router->addRoute(HelloController::class, 'index', '/');
$router->addRoute(HelloController::class, 'hello', '/api');
$router->addRoute(HelloController::class, 'hello', '/api/category/([^/]+)/slug/([^/]+)');
$router->addRoute(HelloController::class, 'hello', '/api/product/:sku');


$app = function ($request, $response) use ($router) {

    // Build request URI
    $uri = 'http://localhost:8000' . $request->getPath();

    // Build a PSR7 ServerRequest
    $serverRequest = new ServerRequest(
        $request->getMethod(),
        $uri
    );
    $action = $router->match('/');

    if (false !== $action) {
        $psrResponse = $action->dispatch($serverRequest);

        $headers = array('Content-Type' => 'text/plain');
        $response->writeHead(200, $headers);
        $response->end($psrResponse->getBody());
    } else {
        echo 'route not found: ' . $uri . PHP_EOL;

        $headers = array('Content-Type' => 'text/plain');
        $response->writeHead(400, $headers);
        $response->end('no route found');
    }
};


$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);
$http = new React\Http\Server($socket);

$http->on('request', $app);
$port = $argv[1];
$socket->listen($port);
echo 'starting server on port:' . $port . PHP_EOL;
$loop->run();
