Tumbleweed Fast Web Framework
==============================

Experiments with different components for a flexible fast "framework"


Performance testing
--------------------

So far on a laptop:

Accessing a controller that returns json  from an object. Simple path /api/health, the return includes php version

```bash
jose@laptop:~/$ wrk -t1 -c50 -d60s http://localhost:80/index.php
Running 1m test @ http://localhost:80/index.php
  1 threads and 50 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.68ms    2.03ms  21.82ms   78.50%
    Req/Sec    20.17k   802.30    21.43k    91.33%
  1204571 requests in 1.00m, 298.62MB read
Requests/sec:  20071.62
Transfer/sec:      4.98MB
```

and with low traffic Latency goes down to just over 300 nanoseconds

```bash
jose@laptop:~/$ wrk -t1 -c10 -d60s http://localhost:80/index.php
Running 1m test @ http://localhost:80/index.php
  1 threads and 10 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   630.60us  324.78us  10.76ms   84.25%
    Req/Sec    16.08k   451.35    17.46k    81.17%
  960076 requests in 1.00m, 238.01MB read
Requests/sec:  16001.11
Transfer/sec:      3.97MB
```

Improvements:

HTTP Keep alive missing for reusing connections
Unix sockets for faster connection and more throutput 
