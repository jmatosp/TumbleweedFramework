<?php

namespace JPinto\Tumbleweed\Controllers;

use GuzzleHttp\Psr7\Response;
use JPinto\TumbleweedCache\CacheFactory;
use Psr\Http\Message\ServerRequestInterface;

class HelloController
{
    public function index(ServerRequestInterface $request)
    {
        $content = json_encode([
            'status' => 'ok',
            'php_version' => PHP_VERSION,
            'os ' => PHP_OS
        ]);

        $response = new Response(200, [], $content);
        $response = $response->withHeader('token', '1234');

        return $response;
    }

    public function hello(ServerRequestInterface $request)
    {
        $content = json_encode([
            'status' => 'ok',
            'sku' => $request->getAttribute('sku')
        ]);

        $response = new Response(200, [], $content);

        return $response->withHeader('token', '1234');
    }
}
