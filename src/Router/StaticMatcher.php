<?php

namespace JPinto\Tumbleweed\Router;

use Psr\Http\Message\RequestInterface;

class StaticMatcher implements MatcherInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * StaticMatcher constructor.
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @param Route $route
     * @return bool
     */
    public function isEligible(Route $route)
    {
        return (
            $this->request->getMethod() == $route->getMethod() &&
            $this->request->getUri()->getPath() == $route->getUri()
        );
    }
}