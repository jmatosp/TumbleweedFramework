<?php

namespace JPinto\Tumbleweed\Router;

class Route
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $uri;

    /**
     * Route constructor.
     * @param $class
     * @param $method
     * @param $uri
     */
    public function __construct($class, $method, $uri)
    {
        $this->class = $class;
        $this->method = $method;
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }
}
