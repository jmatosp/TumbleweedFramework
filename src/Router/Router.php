<?php

namespace JPinto\Tumbleweed\Router;

class Router
{
    /**
     * @var Route[]
     */
    private $routes;

    /**
     * @var array
     */
    private $cachedRoutes;

    /**
     * adds a new route
     *
     * @param $class
     * @param $method
     * @param $uri
     */
    public function addRoute($class, $method, $uri)
    {
        $this->routes[] = new Route($class, $method, $uri);
    }

    /**
     * find the first route for a path
     *
     * @param string $path
     * @return Action
     */
    public function match($path)
    {
        // quick match from memory
        if (isset($this->cachedRoutes[$path])) {
            return $this->cachedRoutes[$path];
        }

        foreach ($this->routes as $route) {
            $route_regex = preg_replace('@:[^/]+@', '([^/]+)', $route->getUri());
            if (preg_match('@^' . $route_regex  . '$@', $path, $matches)) {

                // fix matches
                $pathParams = [];
                preg_match('@:[^/]+@', $route->getUri(), $params);
                foreach ($params as $key => $value) {
                    $pathParams[substr($value, 1)] = $matches[$key+1];
                }

                $action = new Action($pathParams, $route);

                // store in cache
                $this->cachedRoutes[$path] = $action;

                return $action;
            };
        }

        return false;
    }
}
