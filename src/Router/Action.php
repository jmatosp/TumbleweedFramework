<?php

namespace JPinto\Tumbleweed\Router;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Action
{
    /**
     * @var array
     */
    private $params;

    private $route;

    /**
     * Action constructor.
     * @param array $params
     * @param $route
     */
    public function __construct(array $params, Route $route)
    {
        $this->params = $params;
        $this->route = $route;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function dispatch(ServerRequestInterface $request)
    {
        $class = $this->route->getClass();
        $method = $this->route->getMethod();
        $controller = new $class;

        foreach ($this->params as $key => $value) {
            $request = $request->withAttribute($key, $value);
        }

        return $controller->$method($request);
    }
}