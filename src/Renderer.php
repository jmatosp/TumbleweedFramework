<?php

namespace JPinto\Tumbleweed;

use Psr\Http\Message\ResponseInterface;

class Renderer implements RendererInterface
{
    public static function render(ResponseInterface $response)
    {
        foreach ($response->getHeaders() as $name => $header) {
            header($name . ': ' . $header[0]);
        };
        http_response_code($response->getStatusCode());
        echo $response->getBody();

    }
}