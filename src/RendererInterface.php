<?php

namespace JPinto\Tumbleweed;

use Psr\Http\Message\ResponseInterface;

interface RendererInterface
{
    /**
     * @param ResponseInterface $response
     * @return void
     */
    public static function render(ResponseInterface $response);
}